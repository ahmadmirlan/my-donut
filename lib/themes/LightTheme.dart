import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:my_donut/helper/color_helper.dart';

ThemeData lightTheme = ThemeData(
    brightness: Brightness.light,
    scaffoldBackgroundColor: ColorHelper.white,
    highlightColor: Colors.transparent,
    splashColor: Colors.transparent,
    primarySwatch: Colors.amber,
    textTheme: GoogleFonts.latoTextTheme().copyWith(
    ),
);
