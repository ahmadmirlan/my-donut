import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_donut/controller/order_controller.dart';
import 'package:my_donut/helper/color_helper.dart';
import 'package:my_donut/models/model.dart';

class FoodCardWidget extends StatelessWidget {
  final MenuModel food;

  const FoodCardWidget({Key? key, required this.food}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = Get.width;
    final height = Get.height;
    return GetBuilder<OrderController>(builder: (controller) {
      final isOrdered = controller.isMenuHaveAdded(food);
      return Container(
        padding: EdgeInsets.all(width * 0.03),
        margin: EdgeInsets.only(bottom: width * 0.05),
        width: width * 0.42,
        height: height * 0.15,
        decoration: BoxDecoration(
            color: ColorHelper.white,
            border: Border.all(color: ColorHelper.primary, width: 0.5),
            borderRadius: BorderRadius.circular(5)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(width * 0.02),
                  margin: EdgeInsets.only(right: width * 0.05),
                  decoration: BoxDecoration(
                      color: food.color.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(5)),
                  child: Image.asset('assets/icons/${food.icon}',
                      height: width * 0.25, fit: BoxFit.cover),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(food.name,
                        style: TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 20,
                            color: ColorHelper.dark)),
                    Text(food.tagline,
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 13,
                            color: ColorHelper.grey)),
                    Text('\$ ${food.price.toStringAsFixed(2)}',
                        style: const TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600)),
                  ],
                ),
              ],
            ),
            SizedBox(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                        color: isOrdered
                            ? ColorHelper.whiteDarker
                            : ColorHelper.primary,
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(50))),
                    child: Container(
                      margin: const EdgeInsets.only(left: 10, top: 5),
                      child: isOrdered
                          ? Icon(
                              Icons.done,
                              size: 25,
                              color: ColorHelper.white,
                            )
                          : GestureDetector(
                              onTap: () {
                                controller.addMenuToList(food, 1);
                              },
                              child: Icon(
                                Icons.add,
                                size: 25,
                                color: ColorHelper.white,
                              ),
                            ),
                    )),
              ],
            ))
          ],
        ),
      );
    });
  }
}
