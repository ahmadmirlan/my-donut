import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:my_donut/helper/color_helper.dart';

class LogoWidget extends StatelessWidget {
  const LogoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Every Bites',
              style: GoogleFonts.anton(
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                  color: ColorHelper.primary)),
          Text('Are Incredibly Insane!',
              style: GoogleFonts.mouseMemoirs(
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  color: ColorHelper.dark)),
        ],
      ),
    );
  }
}
