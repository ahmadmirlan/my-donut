class AppRoutes {
  static const String MENU = '/menus';
  static const String MENU_DETAIL = '/menu-detail';
  static const String ORDER = '/orders';
  static const String ORDER_COMPLETED = '/order-completed';
}
