import 'package:get/get.dart';
import 'package:my_donut/data/dummy_data.dart';
import 'package:my_donut/models/model.dart';

class MenuController extends GetxController {
  List<MenuModel> _allFoods = [];
  List<CategoryModel> _categories = [];
  int _itemCount = 1;
  late MenuModel _selectedFood;
  late CategoryModel _selectedCategory;
  final _isLoading = false.obs;

  List<MenuModel> get getAllFoods => _allFoods
      .where((element) => element.category.id == _selectedCategory.id)
      .toList();

  List<CategoryModel> get getAllCategories => _categories;

  MenuModel get selectedFood => _selectedFood;

  CategoryModel get selectedCategory => _selectedCategory;

  bool get isLoading => _isLoading.value;

  int get itemCount => _itemCount;

  @override
  void onInit() {
    super.onInit();
    initData();
  }

  void initData() async {
    _isLoading.value = true;
    await Future.delayed(const Duration(seconds: 2), () {
      _allFoods = DummyData.foods;
      _categories = DummyData.categories;
      _selectedCategory = _categories[0];
      _isLoading.value = false;
    });
    update();
  }

  void setSelectedFood(MenuModel food) {
    _selectedFood = food;
    update();
  }

  void setSelectedCategory(CategoryModel category) {
    _selectedCategory = category;
    update();
  }

  void setLoading(bool isLoading) {
    _isLoading.value = isLoading;
  }

  void increaseItem() {
    _itemCount++;
    update();
  }

  void decreaseItem() {
    if(_itemCount > 1) {
      _itemCount--;
      update();
    }
  }

  void resetItemCount() {
    _itemCount = 1;
    update();
  }
}
