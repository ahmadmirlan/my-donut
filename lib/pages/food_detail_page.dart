import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:my_donut/controller/menu_controller.dart';
import 'package:my_donut/data/dummy_data.dart';
import 'package:my_donut/helper/color_helper.dart';

class FoodDetailPage extends StatelessWidget {
  const FoodDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MenuController>(builder: (controller) {
      final food = controller.selectedFood;
      final width = Get.width;
      final height = Get.height;
      int itemCount = 1;
      return Container(
        decoration: BoxDecoration(color: ColorHelper.white),
        child: ListView(
          physics: const BouncingScrollPhysics(),
          children: [
            Container(
              color: ColorHelper.whiteDarker,
              child: Center(
                child: Image.asset(
                  'assets/icons/${food.icon}',
                  height: height * 0.38,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: width * 0.05, right: width * 0.05),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: height * 0.015),
                        child: Text(food.name,
                            style: const TextStyle(
                                fontWeight: FontWeight.w900, fontSize: 20)),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: height * 0.015),
                        child: Text(food.tagline,
                            style: TextStyle(
                                color: ColorHelper.grey,
                                fontWeight: FontWeight.w600,
                                fontSize: 18)),
                      ),
                    ],
                  ),
                  Text('\$ ${food.price}',
                      textAlign: TextAlign.start,
                      style: GoogleFonts.concertOne(
                          color: ColorHelper.dark,
                          fontSize: 25,
                          fontWeight: FontWeight.w900))
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: height * 0.03),
              padding: EdgeInsets.only(left: width * 0.05, right: width * 0.05),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: width * 0.03),
                        child: Icon(Icons.schedule, color: ColorHelper.primary),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Delivery Time', style: TextStyle(
                            color: ColorHelper.grey,
                            fontWeight: FontWeight.w600
                          )),
                          Container(
                            margin: EdgeInsets.only(top: height * 0.01),
                            child: Text('30 min', style: TextStyle(
                                fontSize: 20,
                                color: ColorHelper.dark,
                                fontWeight: FontWeight.w600
                            )),
                          )
                        ],
                      )
                    ],
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          controller.decreaseItem();
                        },
                        child: Container(
                          padding: const EdgeInsets.all(2),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: ColorHelper.secondary),
                          child: Icon(Icons.remove,
                              color: ColorHelper.primary),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(3),
                        margin: EdgeInsets.only(
                            left: Get.width * 0.02,
                            right: Get.width * 0.02),
                        child: Text(controller.itemCount.toString(),
                            style: TextStyle(
                                color: ColorHelper.dark, fontSize: 22)),
                      ),
                      GestureDetector(
                        onTap: () {
                          controller.increaseItem();
                        },
                        child: Container(
                          padding: const EdgeInsets.all(2),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: ColorHelper.primary),
                          child:
                          Icon(Icons.add, color: ColorHelper.white),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: width * 0.05, right: width * 0.05),
              margin:
                  EdgeInsets.only(top: height * 0.04, bottom: height * 0.01),
              child: Text('About',
                  style: TextStyle(
                      color: ColorHelper.dark.withOpacity(0.8),
                      fontWeight: FontWeight.w600,
                      fontSize: 18)),
            ),
            Container(
              padding: EdgeInsets.only(left: width * 0.05, right: width * 0.05),
              margin: EdgeInsets.only(bottom: height * 0.03),
              child: Text(DummyData.descriptionText,
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: ColorHelper.dark.withOpacity(0.8),
                      fontWeight: FontWeight.w400,
                      fontSize: 16)),
            )
          ],
        ),
      );
    });
  }
}
