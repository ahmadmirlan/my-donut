import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:my_donut/controller/menu_controller.dart';
import 'package:my_donut/routes/AppRoutes.dart';
import 'package:my_donut/widgets/default_loading_widget.dart';
import 'package:my_donut/widgets/menu/filter_widget.dart';
import 'package:my_donut/widgets/menu/food_card_widget.dart';
import 'package:my_donut/widgets/menu/search_widget.dart';
import 'package:my_donut/widgets/menu/logo_widget.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MenuController>(builder: (controller) {
      return Obx(() => controller.isLoading
          ? const DefaultLoadingWidget()
          : Column(
              children: [
                Container(
                  padding: EdgeInsets.only(
                      left: Get.width * 0.05, right: Get.width * 0.05),
                  margin: EdgeInsets.only(
                      bottom: Get.height * 0.03, top: Get.height * 0.03),
                  child: const SearchWidget(),
                ),
                Container(
                  height: 50,
                  margin: EdgeInsets.only(bottom: Get.height * 0.03),
                  child: ListView(
                    physics: const BouncingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    children: List.generate(controller.getAllCategories.length,
                        (index) {
                      return GestureDetector(
                        onTap: () {
                          if (controller.selectedCategory.id ==
                              controller.getAllCategories[index].id) {
                            return;
                          }
                          context.loaderOverlay
                              .show(widget: const DefaultLoadingWidget());
                          Future.delayed(const Duration(seconds: 1), () {
                            controller.setSelectedCategory(
                                controller.getAllCategories[index]);
                            context.loaderOverlay.hide();
                          });
                        },
                        child: FilterCategoriesWidget(
                            isSelected: controller.getAllCategories[index].id ==
                                controller.selectedCategory.id,
                            filter: controller.getAllCategories[index]),
                      );
                    }),
                  ),
                ),
                Expanded(child: Container(
                  padding: EdgeInsets.only(
                      left: Get.width * 0.05, right: Get.width * 0.05),
                  child: ListView(
                    physics: const BouncingScrollPhysics(),
                    children:
                    List.generate(controller.getAllFoods.length, (index) {
                      return GestureDetector(
                        onTap: () {
                          controller
                              .setSelectedFood(controller.getAllFoods[index]);
                          context.loaderOverlay
                              .show(widget: const DefaultLoadingWidget());
                          Future.delayed(const Duration(seconds: 1), () {
                            Get.toNamed(AppRoutes.MENU_DETAIL);
                            context.loaderOverlay.hide();
                          });
                        },
                        child:
                        FoodCardWidget(food: controller.getAllFoods[index]),
                      );
                    }),
                  ),
                ))
              ],
            ));
    });
  }
}
