import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_donut/controller/menu_controller.dart';
import 'package:my_donut/controller/order_controller.dart';
import 'package:my_donut/helper/color_helper.dart';
import 'package:my_donut/pages/food_detail_page.dart';
import 'package:my_donut/routes/AppRoutes.dart';
import 'package:my_donut/widgets/order/food_order_widget.dart';

class FoodDetailScreen extends StatelessWidget {
  const FoodDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<OrderController>();
    final menuController = Get.find<MenuController>();
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ColorHelper.whiteDarker,
        leading: Container(
          padding: EdgeInsets.only(left: Get.width * 0.02, top: 5, bottom: 5),
          child: IconButton(
            onPressed: () {
              menuController.resetItemCount();
              Get.back();
            },
            icon: Icon(Icons.arrow_back_rounded,
                color: ColorHelper.dark, size: 25),
          ),
        ),
        actions: [
          Container(
            padding: EdgeInsets.only(right: Get.width * 0.03),
            child: GestureDetector(
              onTap: () {
                menuController.resetItemCount();
                Get.toNamed(AppRoutes.ORDER);
              },
              child: Badge(
                position: const BadgePosition(top: 1, end: -2),
                badgeColor: Colors.redAccent,
                badgeContent: Text('${controller.getOrderedMenu.length}',
                    style: TextStyle(color: ColorHelper.white)),
                child: Icon(
                  Icons.local_grocery_store_outlined,
                  color: ColorHelper.grey,
                  size: 30,
                ),
              ),
            )
            ,
          )
        ],
      ),
      body: const FoodDetailPage(),
      bottomNavigationBar: const FoodOrderWidget(),
    );
  }
}
