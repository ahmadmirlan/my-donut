import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_donut/helper/color_helper.dart';
import 'package:my_donut/pages/dashboard_page.dart';
import 'package:my_donut/widgets/menu/menu_bar_widget.dart';
import 'package:my_donut/widgets/menu/logo_widget.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: ColorHelper.white,
          leading: Container(
              padding: EdgeInsets.only(left: Get.width * 0.03),
              child: const LogoWidget()),
          leadingWidth: 200,
          actions: [
            Container(
                width: 40,
                margin: EdgeInsets.only(right: Get.width * 0.04),
                padding: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: ColorHelper.dark)),
                child: Image.asset('assets/icons/leonardo.png'))
          ],
        ),
        body: const DashboardPage(),
        bottomNavigationBar: const MenuBarWidget());
  }
}
