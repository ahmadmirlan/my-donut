import 'package:my_donut/helper/color_helper.dart';
import 'package:my_donut/helper/color_helper.dart';
import 'package:my_donut/helper/color_helper.dart';
import 'package:my_donut/helper/color_helper.dart';
import 'package:my_donut/helper/color_helper.dart';
import 'package:my_donut/helper/color_helper.dart';
import 'package:my_donut/helper/color_helper.dart';
import 'package:my_donut/models/model.dart';

class DummyData {
  static List<MenuModel> foods = [
    MenuModel(1, 'Chocolate Donut', 'donut1.png', 0.40, categories[0],
        'Spicy Tasty Donut Family', ColorHelper.pink),
    MenuModel(2, 'Pink Donut', 'donut2.png', 0.40, categories[0],
        'Spicy Tasty Donut Family', ColorHelper.pink),
    MenuModel(3, 'Mess Donut', 'donut3.png', 0.35, categories[0],
        'Spicy Tasty Donut Family', ColorHelper.pink),
    MenuModel(4, 'Full Mess Donut', 'donut4.png', 0.50, categories[0],
        'Spicy Tasty Donut Family', ColorHelper.pink),
    MenuModel(5, 'Sweat Donut', 'donut5.png', 0.40, categories[0],
        'Spicy Tasty Donut Family', ColorHelper.pink),
    MenuModel(6, 'Cream Donut', 'donut6.png', 0.40, categories[0],
        'Spicy Tasty Donut Family', ColorHelper.pink),
    MenuModel(7, 'Red Donut', 'donut8.png', 0.35, categories[0],
        'Spicy Tasty Donut Family', ColorHelper.pink),
    MenuModel(8, 'Brown Boba Ice', 'boba4.png', 0.9, categories[1],
        'Fresh and Sweat Tasty', ColorHelper.blue),
    MenuModel(9, 'Chocolate Boba', 'boba5.png', 0.85, categories[1],
        'Fresh and Sweat Tasty', ColorHelper.blue),
    MenuModel(10, 'Blue Ice', 'boba3.png', 0.85, categories[1],
        'Fresh and Sweat Tasty', ColorHelper.blue),
    MenuModel(11, 'Watermelon Ice', 'boba1.png', 0.9, categories[1],
        'Fresh and Sweat Tasty', ColorHelper.blue),
    MenuModel(12, 'Lemon Ice', 'boba2.png', 0.9, categories[1],
        'Fresh and Sweat Tasty', ColorHelper.blue),
    MenuModel(13, 'Bean Coffee', 'coffee1.png', 1.2, categories[2],
        'Hot and Fresh Coffee', ColorHelper.brown),
    MenuModel(14, 'Original Coffee', 'coffee2.png', 1.2, categories[2],
        'Hot and Fresh Coffee', ColorHelper.brown),
    MenuModel(15, 'Milky Coffee', 'coffee3.png', 1.2, categories[2],
        'Hot and Fresh Coffee', ColorHelper.brown),
    MenuModel(16, 'Black Coffee', 'coffee4.png', 1.2, categories[2],
        'Hot and Fresh Coffee', ColorHelper.brown),
  ];

  static List<CategoryModel> categories = [
    CategoryModel(1, 'Donut'),
    CategoryModel(2, 'Soft Drink'),
    CategoryModel(3, 'Coffee'),
  ];

  static List<IngredientsModel> ingredients = [
    IngredientsModel(1, 'Rice', 'rice.png'),
    IngredientsModel(2, 'Tuna', 'tuna.png'),
    IngredientsModel(3, 'Salmon', 'salmon.png'),
    IngredientsModel(4, 'Ebi', 'ebi.png'),
    IngredientsModel(5, 'Mushroom', 'mushroom.png'),
    IngredientsModel(6, 'Cheese', 'cheese.png'),
  ];

  static List<IngredientsModel> drinksRecipe = [
    IngredientsModel(1, 'Cola', 'can.png'),
    IngredientsModel(2, 'Sugar', 'sugar.png'),
    IngredientsModel(3, 'Water', 'water.png'),
    IngredientsModel(4, 'Lime', 'lime.png'),
  ];

  static List<PaymentModel> payments = [
    PaymentModel(1, 'Gopay', 'gopay.png'),
    PaymentModel(2, 'Shopee Pay', 'shopee.png'),
    PaymentModel(3, 'Paypal', 'paypal.png'),
    PaymentModel(4, 'Credit Card', 'credit.png'),
  ];

  static String descriptionText =
      "We want to be your partner for all your food and drink needs. We offer a huge selection of fresh food products for your everyday life, including meat, fresh fish, fruit and vegetables as well as dairy products and snacks.";
}
