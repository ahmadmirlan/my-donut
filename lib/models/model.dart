import 'package:flutter/material.dart';

class MenuModel {
  int id;
  String name;
  String icon;
  double price;
  CategoryModel category;
  String tagline;
  Color color;

  MenuModel(this.id, this.name, this.icon, this.price, this.category, this.tagline, this.color);
}

class CategoryModel {
  int id;
  String categoryName;

  CategoryModel(this.id, this.categoryName);
}

class IngredientsModel {
  int id;
  String name;
  String icon;

  IngredientsModel(this.id, this.name, this.icon);
}

class OrderedMenuModel {
  late MenuModel menu;
  late int quantity;

  OrderedMenuModel(this.menu, this.quantity);
}

class PaymentModel {
  int id;
  String name;
  String icon;

  PaymentModel(this.id, this.name, this.icon);
}
